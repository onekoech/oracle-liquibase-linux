# diffChangeLog

The diffChangeLog command allows you to receive information on differences between two databases you are comparing and creates a changelog file containing deployable changesets.

The diffChangeLog command points out the differences in general and generates changes to resolve most of them.

## Uses

The diffChangeLog command is typically used when you want to create a deployable changelog to synchronize multiple databases. The diffChangeLog command also provides more information about:

- Missing objects in your database
- Changes made to your database
- Unexpected items in your database

## Running the diffChangeLog command

Running the diffChangeLog command requires two URLs:

- **referenceURL** the source for the comparison. The referenceURL attribute represents your source (reference) database which is the starting point and the basis for the database you want to compare.
- **url** – the target of the comparison. The URL attribute stands for your target database which you want to compare to the source (reference) database. You typically perform actions and run the commands and against this database.

## Setup

`changeLogFile=dbchangelog.xml` \
`url:"jdbc:oracle:thin:@<IP OR HOSTNAME>:<PORT>:<SERVICE NAME OR SID>"`\
`username:<USERNAME>`\
`password:<PASSWORD>`\
`referenceUrl:"jdbc:oracle:thin:@<IP OR HOSTNAME>:<PORT>:<SERVICE NAME OR SID>"`\
`referenceUsername:<USERNAME>` \
`referencePassword:<PASSWORD>` \

## Output

The diffChangeLog command produces a list of all Objects and creates a changelog with a list of changesets.

Liquibase Community diffChangeLog categories:

- Catalog
- Column
- Foreign Key
- Index
- Primary Key
- Schema
- Sequence
- Procedure
- Unique Constraints
- View
